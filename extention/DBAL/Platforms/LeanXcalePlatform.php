<?php

namespace Doctrine\Extension\LeanXcale\DBAL\Platforms;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Identifier;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\DBAL\Schema\Sequence;
use Doctrine\DBAL\Schema\TableDiff;
use Doctrine\Deprecations\Deprecation;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\Keywords\LeanXcaleKeywordList;

class LeanXcalePlatform extends AbstractPlatform
{

    /**
     * @inheritDoc
     */
    public function getBooleanTypeDeclarationSQL(array $column): string
    {
        return 'BOOLEAN';
    }

    /**
     * @inheritDoc
     */
    public function getIntegerTypeDeclarationSQL(array $column): ?string
    {
        return 'INT' . $this->_getCommonIntegerTypeDeclarationSQL($column);
    }

    /**
     * {@inheritDoc}
     */
    protected function _getCommonIntegerTypeDeclarationSQL(array $column): string
    {

        return '';
    }

    /**
     * @inheritDoc
     */
    public function getBigIntTypeDeclarationSQL(array $column): string
    {
        return 'BIGINT' . $this->_getCommonIntegerTypeDeclarationSQL($column);

    }

    /**
     * @inheritDoc
     */
    public function getSmallIntTypeDeclarationSQL(array $column): string
    {
        return 'SMALLINT' . $this->_getCommonIntegerTypeDeclarationSQL($column);

    }

    public function getLengthExpression($column)
    {
        return 'CHAR_LENGTH( cast( ' . $column . ' as varchar))';
    }

    /**
     * @inheritDoc
     */
    public function getClobTypeDeclarationSQL(array $column)
    {
        return 'CLOB';
    }

    public function supportsIdentityColumns(): bool
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getDateTimeTypeDeclarationSQL(array $column): string
    {
        return 'TIMESTAMP';
    }

    public function getListTablesSQL(): string
    {
        return <<<SQL
select tableName as table_name
         from sysmeta.tables where tableType = 'TABLE' 
                               and tableCat = {$this->getCurrentDatabaseExpression()}
SQL;
    }

    /**
     * @inheritDoc
     */
    public function getCurrentDatabaseExpression(): string
    {
        return 'currentCatalog()';
    }

    public function getForUpdateSQL()
    {
        return '';
    }

    public function getListTableColumnsSQL($table, $database = null): string
    {

        [$catalog, $table] = $this->getTableAndSchema($table, $database);
        $where = sprintf("col.tableCat = %s  and col.tableName in (%s)", $catalog, $this->getIdentifierCasesStr($table));

        //        echo $sql;
        return <<<SQL
 select col.columnName as field, 
        col.typeName as type ,
        col.dataType as data_type,
        col.nullable as is_nullable,
        col.remarks as comment,
        col.columnSize as column_size,
        col.decimalDigits as decimal_digits,
        col.columnDef as column_default,
        col.isAutoincrement as is_autoincrement,
        col.isGeneratedcolumn as is_generated_column,
        col.tableName as table_name,
        col.tableCat as table_catalog
        
 from sysmeta.columns col
 where $where and col.columnName <> '_UUID_'
 order by ordinalPosition
 
SQL;
    }

    private function getTableAndSchema($table, $database = "currentCatalog()"): array
    {
        $databaseExpression = $this->getCurrentDatabaseExpression();
        $catalog = !empty($database) ? $database : $databaseExpression;
        if (strpos($table, '.') !== false) {
            [$catalog, $table] = explode('.', $table);
        }
        $catalog = $catalog !== $databaseExpression ? $this->quoteStringLiteral($catalog) : $databaseExpression;
        $table = new Identifier($table);
        $table = $this->quoteStringLiteral($table->getName());
        return [$catalog, $table];
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'leanxcale';
    }

    public function getIdentifierCasesStr($identifier): string
    {
        return implode(",", $this->getIdentifierCases($identifier));
    }

    public function getIdentifierCases($identifier): array
    {
        return [$identifier, strtolower($identifier), strtoupper($identifier)];
    }

    public function getEmptyIdentityInsertSQL($quotedTableName, $quotedIdentifierColumnName)
    {
        return 'INSERT INTO ' . $quotedTableName . ' (' . $quotedIdentifierColumnName . ') VALUES (DEFAULT)';
    }

    /**
     * Returns the FOREIGN KEY query section dealing with non-standard options
     * as MATCH, INITIALLY DEFERRED, ON UPDATE, ...
     *
     * @param ForeignKeyConstraint $foreignKey The foreign key definition.
     *
     * @return string
     */
    public function getAdvancedForeignKeyOptionsSQL(ForeignKeyConstraint $foreignKey): string
    {
        return '';
    }

    /**
     * @param string $table
     *
     * @return string
     *
     * @throws Exception If not supported on this platform.
     * @deprecated The SQL used for schema introspection is an implementation detail and should not be relied upon.
     *
     */
    public function getListTableForeignKeysSQL($table): string
    {
        [$catalog, $table] = $this->getTableAndSchema($table);
        $where = sprintf("fktableCat = %s and fktableName in (%s)", $catalog, $this->getIdentifierCasesStr($table));

        return <<<SQL
select pktableCat as pk_table_catalog, 
       pktableName as pk_table_name, 
       pkcolumnName as pk_column_name, 
       pkName as pk_name,
       fktableCat as fk_table_catalog,
       fktableName as fk_table_name,
       fkcolumnName as fk_column_name,
       fkName as fk_name
from LXSYSMETA.FOREIGN_KEYS
where $where
order by keySeq
        
SQL;
//        NOUSR.BUG_PRODUCT

    }

    public function getListViewsSQL($database)
    {
        return <<<SQL
select st.tableName as table_name, lv.query as definition
         from sysmeta.tables st
         join LXSYSMETA.VIEW_TABLES lv on st.tableName = lv.tableName
         where st.tableType = 'VIEW' 
                               and st.tableCat = {$this->getCurrentDatabaseExpression()}
SQL;
    }

    public function getListTableIndexesSQL($table, $database = null): string
    {
        [$schema, $table] = $this->getTableAndSchema($table, $database);
        $where = sprintf("tableCat = %s and tableName in (%s)", $schema, $this->getIdentifierCasesStr($table));
        $fkWhere = sprintf("fktableCat = %s and fktableName in (%s)", $schema, $this->getIdentifierCasesStr($table));
        return <<<SQL
select * from (select  indexName as key_name ,nonUnique as non_unique, 
        columnName as column_name, false as "primary", ordinalPosition as "position"
from lxsysmeta.indexes 
 where $where 
   and indexName not in (select fkName from LXSYSMETA.FOREIGN_KEYS where $fkWhere)
 order by ordinalPosition)
 union
 select * from (select pkName as key_name, false as non_unique,
        columnName as column_name, true as "primary", keySeq as "position"
     from LXSYSMETA.PRIMARY_KEYS 
        where $where and columnName <> '_UUID_'
order by keySeq) order by "position"
SQL;
    }

    public function supportsCreateDropDatabase(): bool
    {
        return false;
    }


    public function getLocateExpression($str, $substr, $startPos = false)
    {
        if ($startPos !== false) {
            $str = $this->getSubstringExpression($str, $startPos);

            return 'CASE WHEN (POSITION(' . $substr . ' IN ' . $str . ') = 0) THEN 0'
                . ' ELSE (POSITION(' . $substr . ' IN ' . $str . ') + ' . $startPos . ' - 1) END';
        }

        return 'POSITION(' . $substr . ' IN ' . $str . ')';
    }


    public function getDateTypeDeclarationSQL(array $column)
    {
        return 'DATE';
    }

    public function getTimeTypeDeclarationSQL(array $column)
    {
        return 'TIME';
    }

    public function getDateTimeTzFormatString(): string
    {
        return 'Y-m-d H:i:s.u';
    }

    public function getDateTimeFormatString(): string
    {
        return 'Y-m-d H:i:s.u';
    }

    public function supportsSavepoints()
    {
        return false;
    }

    public function getTruncateTableSQL($tableName, $cascade = false)
    {
        $tableIdentifier = new Identifier($tableName);
        $sql = 'TRUNCATE TABLE ' . $tableIdentifier->getQuotedName($this);

        if ($cascade) {
            $sql .= ' CASCADE';
        }

        return $sql;
    }

    public function getDateDiffExpression($date1, $date2): string
    {
        return 'ABS(TIMESTAMPDIFF(DAY,cast(' . $date1 . ' as date), cast(' . $date2 . ' as date)))';
    }

    public function getAlterTableSQL(TableDiff $diff): array
    {
        $sql = [];
        $columnSql = [];

        $tableName = $diff->getName($this)->getQuotedName($this);
        foreach ($diff->addedColumns as $column) {
            if ($this->onSchemaAlterTableAddColumn($column, $diff, $columnSql)) {
                continue;
            }

            $sql[] = $this->getAddColumnSQl($tableName, $column);

        }

        foreach ($diff->removedColumns as $column) {
            if ($this->onSchemaAlterTableRemoveColumn($column, $diff, $columnSql)) {
                continue;
            }

            $sql[] = $this->getDropColumnSQl($tableName, $column);

        }

        foreach ($diff->changedColumns as $columnDiff) {
            if ($this->onSchemaAlterTableChangeColumn($columnDiff, $diff, $columnSql)) {
                continue;
            }


            $newColumn = $columnDiff->column;

            $oldColumnName = $columnDiff->getOldColumnName()->getQuotedName($this);
            if ($columnDiff->hasChanged('default') && !is_null($newColumn->getDefault())) {
                $sql[] = "alter table $tableName alter column  " . $this->getColumnDeclarationSQL($oldColumnName, $newColumn->toArray());
            } else {
                $oldColumn = new Column($this->quoteIdentifier($oldColumnName), $newColumn->getType());
                $sql = array_merge($sql, $this->getColumnChangeSQL($tableName, $oldColumn, $newColumn));
            }


        }
        foreach ($diff->renamedColumns as $oldColumnName => $column) {
            if ($this->onSchemaAlterTableRenameColumn($oldColumnName, $column, $diff, $columnSql)) {
                continue;
            }

            $newColumn = $column;
            $oldColumn = new Column($this->quoteIdentifier($oldColumnName), $newColumn->getType());
            $sql = array_merge($sql, $this->getColumnChangeSQL($tableName, $oldColumn, $newColumn));

        }

        $tableSql = [];

        if (!$this->onSchemaAlterTable($diff, $tableSql)) {
            $newName = $diff->getNewName();
            if ($newName !== false) {
                $sql[] = sprintf(
                    'ALTER TABLE %s RENAME TO %s',
                    $diff->getName($this)->getQuotedName($this),
                    $newName->getQuotedName($this)
                );
            }

            $sql = array_merge(
                $this->getPreAlterTableIndexForeignKeySQL($diff),
                $sql,
                $this->getPostAlterTableIndexForeignKeySQL($diff)
            );
        }

        return array_merge($sql, $tableSql, $columnSql);

    }

    /**
     * @throws Exception
     */
    private function getAddColumnSQl(string $tableName, Column $column): string
    {

        return 'ALTER TABLE ' . $tableName . ' ADD COLUMN ' . $this->getColumnDeclarationSQL($column->getQuotedName($this), $column->toArray());

    }

    public function getColumnDeclarationSQL($name, array $column): string
    {

        if (isset($column['columnDefinition'])) {
            $declaration = $this->getCustomTypeDeclarationSQL($column);
        } else {
            $default = $this->getDefaultValueDeclarationSQL($column);

            $charset = !empty($column['charset']) ?
                ' ' . $this->getColumnCharsetDeclarationSQL($column['charset']) : '';

            $collation = !empty($column['collation']) ?
                ' ' . $this->getColumnCollationDeclarationSQL($column['collation']) : '';

            $notnull = !empty($column['notnull']) /*&& !($column['type'] instanceof BlobType) */ ? ' NOT NULL' : '';


            $unique = !empty($column['unique']) ?
                ' ' . $this->getUniqueFieldDeclarationSQL() : '';

            $check = !empty($column['check']) ? ' ' . $column['check'] : '';

            $typeDecl = $column['type']->getSQLDeclaration($column, $this);
            $declaration = $typeDecl . $charset . $notnull . $default . $unique . $check . $collation;

            if ($this->supportsInlineColumnComments() && isset($column['comment']) && $column['comment'] !== '') {
                $declaration .= ' ' . $this->getInlineColumnCommentSQL($column['comment']);
            }
        }

        return $name . ' ' . $declaration;
    }

    public function getDefaultValueDeclarationSQL($column): string
    {
        if (!empty($column['autoincrement'])) {
            return ' GENERATED ALWAYS AS IDENTITY';
        }
        return parent::getDefaultValueDeclarationSQL($column);

    }

    private function getDropColumnSQl(string $tableName, Column $column): string
    {

        return 'ALTER TABLE ' . $tableName . ' DROP COLUMN ' . $column->getQuotedName($this);

    }

    /**
     * @throws Exception
     * @throws SchemaException
     */
    private function getColumnChangeSQL(string $tableName, Column $oldColumn, Column $newColumn): array
    {
        $sql = [];


        $newColName = $newColumn->getQuotedName($this);
        $oldColName = $oldColumn->getQuotedName($this);
        $newColType = $newColumn->getType();

        $newColOptions = $this->getColumnOptions($newColumn);
        if (strtolower($oldColumn->getName()) == strtolower($newColName)) {
            $tempName = $this->generateIdentifierName([$newColName, $oldColName], 'temp');
            $tempColumn = new Column($tempName, $newColType, $newColOptions);
            $sql = array_merge($sql,
                $this->getColumnChangeSQL($tableName, $oldColumn, $tempColumn),
                $this->getColumnChangeSQL($tableName, $tempColumn, $newColumn)
            );

        } else {
            $newColDBType = $newColType->getSQLDeclaration($newColOptions, $this);
            $updateSql = "UPDATE $tableName SET $newColName = cast($oldColName as $newColDBType)";

            $sql[] = $this->getAddColumnSQl($tableName, $newColumn);

            $sql[] = $updateSql;
            $sql[] = $this->getDropColumnSQl($tableName, $oldColumn);

        }
        return $sql;

    }

    private function getColumnOptions(Column $column): array
    {
        $colArr = $column->toArray();
        $options = [];
        $keys = [
            'default', 'notnull', 'length',
            'precision', 'scale', 'fixed',
            'unsigned', 'autoincrement',
            'columnDefinition', 'comment',
        ];

        foreach ($keys as $key) {
            $options[$key] = $colArr[$key] ?? null;
        }
        return $options;
    }

    private function generateIdentifierName(array $columnNames, $prefix = '', $maxSize = 30): string
    {
        $hash = implode('', array_map(static function ($column): string {
            return dechex(crc32($column));
        }, $columnNames));

        return strtoupper(substr($prefix . '_' . $hash, 0, $maxSize));
    }

    /**
     * @return string[]
     * @throws SchemaException
     */
    protected function getPreAlterTableIndexForeignKeySQL(TableDiff $diff)
    {
        $tableName = $diff->getName($this)->getQuotedName($this);

        $sql = [];
        if ($this->supportsForeignKeyConstraints()) {
            foreach ($diff->removedForeignKeys as $foreignKey) {
                if ($foreignKey instanceof ForeignKeyConstraint) {
                    $foreignKey = $foreignKey->getQuotedName($this);
                }

                $sql[] = $this->getDropForeignKeySQL($foreignKey, $tableName);
            }

            foreach ($diff->changedForeignKeys as $foreignKey) {
                $key = strtolower($foreignKey->getName());
                if ($this->diffForeignKey($foreignKey, $diff->fromTable->getForeignKey($key))) {
                    $sql[] = $this->getDropForeignKeySQL($foreignKey->getQuotedName($this), $tableName);
                }
            }
        }

        foreach ($diff->removedIndexes as $index) {
            $sql[] = $this->getDropIndexSQL($index->getQuotedName($this), $tableName);
        }

        foreach ($diff->changedIndexes as $index) {
            $sql[] = $this->getDropIndexSQL($index->getQuotedName($this), $tableName);
        }

        return $sql;
    }

    public function getDropForeignKeySQL($foreignKey, $table)
    {
        return $this->getDropConstraintSQL($foreignKey, $table);
    }

    /**
     * @return bool
     */
    public function diffForeignKey(ForeignKeyConstraint $key1, ForeignKeyConstraint $key2)
    {
        if (
            array_map('strtolower', $key1->getUnquotedLocalColumns())
            !== array_map('strtolower', $key2->getUnquotedLocalColumns())
        ) {
            return true;
        }

        if (
            array_map('strtolower', $key1->getUnquotedForeignColumns())
            !== array_map('strtolower', $key2->getUnquotedForeignColumns())
        ) {
            return true;
        }

        if ($key1->getUnqualifiedForeignTableName() !== $key2->getUnqualifiedForeignTableName()) {
            return true;
        }
        return false;

    }

    /**
     * @return string[]
     * @throws SchemaException
     */
    protected function getPostAlterTableIndexForeignKeySQL(TableDiff $diff)
    {
        $sql = [];
        $newName = $diff->getNewName();

        if ($newName !== false) {
            $tableName = $newName->getQuotedName($this);
        } else {
            $tableName = $diff->getName($this)->getQuotedName($this);
        }

        if ($this->supportsForeignKeyConstraints()) {
            foreach ($diff->addedForeignKeys as $foreignKey) {
                $sql[] = $this->getCreateForeignKeySQL($foreignKey, $tableName);
            }

            foreach ($diff->changedForeignKeys as $foreignKey) {
                $key = strtolower($foreignKey->getName());
                if ($this->diffForeignKey($foreignKey, $diff->fromTable->getForeignKey($key))) {
                    $sql[] = $this->getCreateForeignKeySQL($foreignKey, $tableName);

                }
            }
        }

        foreach ($diff->addedIndexes as $index) {
            $sql[] = $this->getCreateIndexSQL($index, $tableName);
        }

        foreach ($diff->changedIndexes as $index) {
            $sql[] = $this->getCreateIndexSQL($index, $tableName);
        }

        foreach ($diff->renamedIndexes as $oldIndexName => $index) {
            $oldIndexName = new Identifier($oldIndexName);
            $sql = array_merge(
                $sql,
                $this->getRenameIndexSQL($oldIndexName->getQuotedName($this), $index, $tableName)
            );
        }

        return $sql;
    }

    public function getCommentOnColumnSQL($tableName, $columnName, $comment)
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    protected function initializeDoctrineTypeMappings()
    {
        $this->doctrineTypeMapping = [
            'boolean' => 'boolean',
            'tinyint' => 'integer',
            'smallint' => 'integer',
            'int' => 'integer',
            'integer' => 'integer',
            'bigint' => 'bigint',
            'numeric' => 'decimal',
            'decimal' => 'decimal',
            'real' => 'float',
            'float' => 'float',
            'double' => 'float',
            'char' => 'string',
            'character' => 'string',
            'varchar' => 'string',
            'character varying' => 'string',
            'binary' => 'binary',
            'varbinary' => 'binary',
            'binary varying' => 'binary',
            'date' => 'date',
            'time' => 'time',
            'timestamp' => 'datetime',
            'blob' => 'blob',
            'clob' => 'text',

        ];
    }

    protected function createReservedKeywordsList(): \Doctrine\DBAL\Platforms\Keywords\KeywordList
    {
        return new LeanXcaleKeywordList();
    }

    /**
     * {@inheritDoc}
     */
    protected function getVarcharTypeDeclarationSQLSnippet($length, $fixed): string
    {
        return $fixed ? ($length > 0 ? 'CHAR(' . $length . ')' : 'CHAR(255)')
            : ($length > 0 ? 'VARCHAR(' . $length . ')' : 'VARCHAR(255)');
    }

    protected function getBinaryTypeDeclarationSQLSnippet($length, $fixed/*, $lengthOmitted = false*/): string
    {

        return $fixed
            ? 'BINARY' : $this->getBlobTypeDeclarationSQL(['length' => $length, "fixed" => $fixed]);
    }

    /**
     * @inheritDoc
     */
    public function getBlobTypeDeclarationSQL(array $column)
    {
        return 'BLOB';
    }

    /**
     * Returns the SQL used to create a table.
     *
     * @param string $name
     * @param mixed[][] $columns
     * @param mixed[] $options
     *
     * @return string[]
     */
    protected function _getCreateTableSQL($name, array $columns, array $options = []): array
    {
        $columnListSql = $this->getColumnDeclarationListSQL($columns);

        if (isset($options['uniqueConstraints']) && !empty($options['uniqueConstraints'])) {
            foreach ($options['uniqueConstraints'] as $index => $definition) {
                $columnListSql .= ', ' . $this->getUniqueConstraintDeclarationSQL($index, $definition);
            }
        }

        if (isset($options['primary']) && !empty($options['primary'])) {
            $columnListSql .= ', PRIMARY KEY(' . implode(', ', array_unique(array_values($options['primary']))) . ')';
        }


        $query = 'CREATE TABLE ' . $name . ' (' . $columnListSql;
        $check = $this->getCheckDeclarationSQL($columns);

        if (!empty($check)) {
            $query .= ', ' . $check;
        }

        $query .= ')';

        $sql = [$query];

        if (isset($options['indexes']) && !empty($options['indexes'])) {
            foreach ($options['indexes'] as $index) {
                $sql[] = $this->getCreateIndexSQL($index, $name);
            }
        }

        if (isset($options['foreignKeys'])) {
            foreach ($options['foreignKeys'] as $definition) {
                $sql[] = $this->getCreateForeignKeySQL($definition, $name);
            }
        }

        return $sql;
    }

    protected function getDateArithmeticIntervalExpression($date, $operator, $interval, $unit)
    {
        return " TIMESTAMPADD($unit,{$operator}{$interval},$date)";
    }


    /**
     *
     * @return string
     */
    public function getDropTemporaryTableSQL($table)
    {
        throw Exception::notSupported(__METHOD__);
    }

    /**
     * @throws Exception
     */
    public function getCreateTemporaryTableSnippetSQL()
    {
        throw Exception::notSupported(__METHOD__);
    }

    /**
     * @throws Exception
     */
    public function getTemporaryTableSQL()
    {
        throw Exception::notSupported(__METHOD__);
    }

    public function getDummySelectSQL()
    {
        $expression = func_num_args() > 0 ? func_get_arg(0) : '1';

        return sprintf('SELECT %s', $expression);
    }


    public function supportsSequences()
    {
        return false;
    }

    private function getSequenceCacheSQL(Sequence $sequence): string
    {
        if ($sequence->getCache() > 1) {
            return ' CACHE ' . $sequence->getCache();
        }

        return '';
    }

    public function getCreateSequenceSQL(Sequence $sequence): string
    {
        return 'CREATE SEQUENCE ' . $sequence->getQuotedName($this) .
            ' START WITH ' . $sequence->getInitialValue() .
            ' INCREMENT BY ' . $sequence->getAllocationSize() .
            ' MINVALUE ' . $sequence->getInitialValue() .
            $this->getSequenceCacheSQL($sequence);
    }

    /**
     * Returns the SQL to change a sequence on this platform.
     *
     * @return string
     *
     * @throws Exception If not supported on this platform.
     */
    public function getAlterSequenceSQL(Sequence $sequence)
    {
        throw Exception::notSupported(__METHOD__);
    }

    public function getListSequencesSQL($database)
    {

        return <<<SQL
select tableName as table_name
         from sysmeta.tables where tableType = 'SEQUENCE' 
                               and tableCat = {$this->getCurrentDatabaseExpression()}
SQL;

        throw Exception::notSupported(__METHOD__);
    }



}

