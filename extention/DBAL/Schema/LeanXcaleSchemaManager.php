<?php

namespace Doctrine\Extension\LeanXcale\DBAL\Schema;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Doctrine\DBAL\Schema\Sequence;
use Doctrine\DBAL\Schema\View;
use Doctrine\DBAL\Types\Type;

class LeanXcaleSchemaManager extends AbstractSchemaManager
{

    /**
     * @inheritDoc
     */
    protected function _getPortableTableColumnDefinition($tableColumn)
    {


        $tableColumn = array_change_key_case($tableColumn, CASE_LOWER);

//        $field = $tableColumn['field'] ?? '';
        $length = $tableColumn['column_size'] == "-1" ? null : $tableColumn['column_size'];
        $comment = $tableColumn['comment'];
        $is_nullable = $tableColumn['is_nullable'] == "1";
//        $data_type = $tableColumn['data_type']; //int values
        $complete_type = strtolower($tableColumn['type']); //data type definition
        $dbType = strtok($complete_type, " (,)");
        $default = $tableColumn['column_default'];
        $fixed = null;
        $scale = null;
        $precision = null;
        $autoincrement = $tableColumn['is_autoincrement'] == 'YES' || $tableColumn['is_generated_column'] == 'YES';

        $type = $this->_platform->getDoctrineTypeMapping($dbType);



        switch ($dbType) {

            case 'boolean':
            case 'bool':
                if ($tableColumn['column_default'] === 'true' || $tableColumn['column_default'] != '0') {
                    $default = true;
                }

                if ($tableColumn['column_default'] === 'false' || $tableColumn['column_default'] == '0') {
                    $default = false;
                }

                $length = null;
                break;

            case 'tinyint':
            case 'smallint':
            case 'int':
            case 'integer':
                $default = is_null($default) ? null : $default;
            //fallthrough
            case 'bigint':
                $length = null;

                break;
            case 'numeric':
            case 'decimal':
                $precision = $length;/*??strtok(" (,)")*/
                $scale = $tableColumn['decimal_digits'];/*??strtok(" (,)")*/
                $length = null;
                $default = is_null($default) ? null : $default;
                break;

            case 'real':
            case 'float':
            case 'double':
                $default = is_null($default) ? null : $default;
                $length = null;
                break;

            case 'blob':
            case 'clob':
                $length = null;
                break;

            case 'char':
            case 'character':
            case 'binary':
                $fixed = true;
                break;


        }


        $options = [
            'length' => $length,
            'notnull' => !$is_nullable,
            'default' => $default,
            'precision' => $precision,
            'scale' => $scale,
            'fixed' => $fixed,
            'unsigned' => false,
            'autoincrement' => $autoincrement,
            'comment' => $comment
        ];

        foreach (['scale', 'precision', 'length'] as $item) {

            if ($options[$item] !== null) {
                $options[$item] = (int)$options[$item];
            }
        }
        $column = new Column( $tableColumn['field'] , Type::getType($type), $options);

        return $column;

    }

    protected function _getPortableTableDefinition($table): string
    {

        return /*$table['SCHEMA_NAME'] . '.' .*/ $table['TABLE_NAME'];
    }

    /**
     * @param mixed[][] $tableForeignKeys
     *
     * @return ForeignKeyConstraint[]
     */
    protected function _getPortableTableForeignKeysList($tableForeignKeys): array
    {
        $list = [];
        foreach ($tableForeignKeys as $value) {
            $value = array_change_key_case($value, CASE_LOWER);
            if (!isset($list[$value['fk_name']])) {


                $list[$value['fk_name']] = [
                    'name' => $value['fk_name'],
                    'local' => [],
                    'foreign' => [],
                    'foreignTable' => $value['pk_table_name'],
                    'onDelete' => null,
                    'onUpdate' => null,
                ];
            }
            $list[$value['fk_name']]['local'][] = $value['fk_column_name'];
            $list[$value['fk_name']]['foreign'][] = $value['pk_column_name'];

        }


        return parent::_getPortableTableForeignKeysList($list);
    }

    protected function _getPortableViewDefinition($view)
    {
        $view = array_change_key_case($view, CASE_LOWER);

        return new View( $view['table_name'], $view['definition']);
    }

    protected function _getPortableTableForeignKeyDefinition($tableForeignKey): ForeignKeyConstraint
    {


        return new ForeignKeyConstraint(
            $tableForeignKey['local'],
            $tableForeignKey['foreignTable'],
            $tableForeignKey['foreign'],
            $this->_platform->quoteIdentifier($tableForeignKey['name']),
            ['onUpdate' => null, 'onDelete' => null]
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function _getPortableTableIndexesList($tableIndexes, $tableName = null): array
    {
        foreach ($tableIndexes as $k => $v) {
            $v = array_change_key_case($v, CASE_LOWER);
            $tableIndexes[$k] = $v;
        }

        return parent::_getPortableTableIndexesList($tableIndexes, $tableName);
    }

    protected function _getPortableSequenceDefinition($sequence)
    {
        throw Exception::notSupported('Sequences');
//        return new Sequence($sequenceName, (int) $sequence['increment_by'], (int) $sequence['min_value']);
    }


}