<?php

namespace Doctrine\Extension\LeanXcale\DBAL;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\API\ExceptionConverter;
use Doctrine\DBAL\Driver\PDO\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\LeanXcalePlatform;
use Doctrine\Extension\LeanXcale\DBAL\Schema\LeanXcaleSchemaManager;
use PDO;
use PDOException;

class LeanXcaleDriver implements \Doctrine\DBAL\Driver
{

    /**
     * @inheritDoc
     */
    public function connect(array $params)
    {
        $options = $params['driverOptions'] ?? [];
        $dsn = "lxodbc:" . ($params["dsn"] ?? $options['dsn'] ?? '');

        try {
            $pdo = new PDO(
                $dsn,
                $params['user'] ?? '',
                $params['password'] ?? '',
                []
            );
        } catch (PDOException $exception) {
            throw Exception::new($exception);
        }

        return new \Doctrine\DBAL\Driver\PDO\Connection($pdo);

    }

    /**
     * @inheritDoc
     */
    public function getDatabasePlatform()
    {
        return new LeanXcalePlatform();
    }

    /**
     * @inheritDoc
     */
    public function getSchemaManager(Connection $conn, AbstractPlatform $platform)
    {
        return new LeanXcaleSchemaManager($conn, $platform);
    }

    /**
     * @inheritDoc
     */
    public function getExceptionConverter(): ExceptionConverter
    {
        return new API\LeanXcaleExceptionConverter();
    }
}