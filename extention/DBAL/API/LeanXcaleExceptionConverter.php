<?php

namespace Doctrine\Extension\LeanXcale\DBAL\API;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception\DatabaseDoesNotExist;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\DBAL\Exception\DriverException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\DBAL\Exception\InvalidFieldNameException;
use Doctrine\DBAL\Exception\NonUniqueFieldNameException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\ServerException;
use Doctrine\DBAL\Exception\SyntaxErrorException;
use Doctrine\DBAL\Exception\TableExistsException;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\DBAL\Query;

class LeanXcaleExceptionConverter implements \Doctrine\DBAL\Driver\API\ExceptionConverter
{

    /**
     * @inheritDoc
     */
    public function convert(Exception $exception, ?Query $query): DriverException
    {
        $message = $exception->getMessage();
        if (preg_match("/Table '.+' already exists/", $message)) {
            return new TableExistsException($exception, $query);
        } else if (preg_match("/: bad key: key not unique/", $message)||preg_match("/uniq\\/upd checks: getreps: already exists/", $message)) {
            return new UniqueConstraintViolationException($exception, $query);

        }else if (preg_match("/Table '.+' not found/", $message)) {
            return new TableNotFoundException($exception, $query);

        }else if (preg_match("/Object '.+' not found/", $message)) {
            return new DatabaseObjectNotFoundException($exception, $query);

        }else if (preg_match("/FK found for key in table/", $message)) {
            return new ForeignKeyConstraintViolationException($exception, $query);

        }else if (preg_match("/Unknown target column '.+'/", $message)) {
            return new InvalidFieldNameException($exception, $query);

        }else if (preg_match("/Column '.+' is ambiguous/", $message)) {
            return new NonUniqueFieldNameException($exception, $query);

        }else if (preg_match("/parse failed/", $message)) {
            return new SyntaxErrorException($exception, $query);

        }else if (preg_match("/Cannot insert null value for column/", $message)) {
            return new NotNullConstraintViolationException($exception, $query);

        } /*else if (stripos($message, 'not found') !== false) {
            return new DatabaseObjectNotFoundException($exception, $query);
        }*/
        return new DriverException($exception, $query);
    }
}