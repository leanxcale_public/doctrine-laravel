<?php

namespace Doctrine\DBAL\Tests\Driver\LeanXcale;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Driver\API\ExceptionConverter;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Tests\Driver\AbstractDriverTest;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\LeanXcalePlatform;
use Doctrine\Extension\LeanXcale\DBAL\Schema\LeanXcaleSchemaManager;

class DriverTest extends AbstractDriverTest
{

    protected function createDriver(): Driver
    {
        return new \Doctrine\Extension\LeanXcale\DBAL\LeanXcaleDriver();
    }

    protected function createPlatform(): AbstractPlatform
    {
        return new LeanXcalePlatform();
    }

    protected function createSchemaManager(Connection $connection): AbstractSchemaManager
    {
        return new LeanXcaleSchemaManager($connection,
            $this->createPlatform());
    }

    protected function createExceptionConverter(): ExceptionConverter
    {
        return  new \Doctrine\Extension\LeanXcale\DBAL\API\LeanXcaleExceptionConverter();
    }
}