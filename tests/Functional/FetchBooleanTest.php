<?php

declare(strict_types=1);

namespace Doctrine\DBAL\Tests\Functional;

use Doctrine\DBAL\Platforms\PostgreSQL94Platform;
use Doctrine\DBAL\Tests\FunctionalTestCase;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\LeanXcalePlatform;

class FetchBooleanTest extends FunctionalTestCase
{
    protected function setUp(): void
    {
        $platform = $this->connection->getDatabasePlatform();
        if ($platform instanceof PostgreSQL94Platform|| $platform instanceof  LeanXcalePlatform) {
            return;
        }

        self::markTestSkipped('Only PostgreSQL supports boolean values natively');
    }

    /**
     * @dataProvider booleanLiteralProvider
     */
    public function testBooleanConversionSqlLiteral(string $literal, bool $expected): void
    {
        self::assertSame([$expected], $this->connection->fetchNumeric(
            $this->connection->getDatabasePlatform()
                ->getDummySelectSQL($literal)
        ));
    }

    /**
     * @return iterable<array{string, bool}>
     */
    public function booleanLiteralProvider(): iterable
    {
        yield ['true', true];
        yield ['false', false];
    }
}
