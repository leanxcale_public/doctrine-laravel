<?php

namespace Doctrine\DBAL\Tests\Functional\Driver\PDO\LeanXcale;

use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Tests\Functional\Driver\AbstractDriverTest;

class DriverTest extends AbstractDriverTest
{

    protected function createDriver(): Driver
    {
      return new \Doctrine\Extension\LeanXcale\DBAL\LeanXcaleDriver();
    }
    protected static function getDatabaseNameForConnectionWithoutDatabaseNameParameter(): ?string
    {
        return null;
    }

    public function testReturnsDatabaseNameWithoutDatabaseNameParameter(): void
    {
        self::markTestSkipped('LeanXcale does not support connecting without database name.');
    }
}