<?php

namespace Doctrine\DBAL\Tests\Functional\Schema;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\AbstractAsset;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\View;
use Doctrine\DBAL\Types\BinaryType;
use Doctrine\DBAL\Types\BlobType;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\LeanXcalePlatform;

class LeanXcaleSchemaManagerTest extends SchemaManagerFunctionalTestCase
{

    public function testListTableColumnsWithFixedStringColumn(): void
    {
        self::markTestSkipped('Fixed is not supported, it is implicitly converted to normal String (Varchar())');
    }

    public function testCommentInTable(): void
    {
        self::markTestSkipped('Comment is not supported');

    }

    protected function assertBinaryColumnIsValid(Table $table, string $columnName, int $expectedLength): void
    {
        $column = $table->getColumn($columnName);
        self::assertInstanceOf(BinaryType::class, $column->getType());
        self::assertTrue($column->getFixed());
    }
    protected function assertVarBinaryColumnIsValid(Table $table, string $columnName, int $expectedLength): void
    {
        $column = $table->getColumn($columnName);
        self::assertInstanceOf(BlobType::class, $column->getType());
//        self::assertSame($expectedLength, $column->getLength());
        self::assertFalse($column->getFixed());
    }


    public function testCreateAndListViews(): void
    {
        if (!$this->connection->getDatabasePlatform()->supportsViews()) {
            self::markTestSkipped('Views is not supported by this platform.');
        }

        $this->createTestTable('view_test_table');

        $name = 'doctrine_test_view';
        $sql = 'SELECT * FROM view_test_table';

        $view = new View($name, $sql);

        $this->schemaManager->createView($view);

        $views = $this->schemaManager->listViews();

        $filtered = array_values($this->filterElementsByName($views, $name));
        self::assertCount(1, $filtered);

        $viewKey = strtolower($filtered[0]->getName());
        self::assertStringContainsStringIgnoringCase('view_test_table', $views[$viewKey]->getSql());
    }

    private function filterElementsByName(array $items, string $name): array
    {
        return array_filter(
            $items,
            static function (AbstractAsset $item) use ($name): bool {
                return $item->getShortestName($item->getNamespaceName()) === $name;
            }
        );
    }

    protected function supportsPlatform(AbstractPlatform $platform): bool
    {
        return $platform instanceof LeanXcalePlatform;
    }
}