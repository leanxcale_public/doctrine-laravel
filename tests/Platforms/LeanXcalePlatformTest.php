<?php

namespace Doctrine\DBAL\Tests\Platforms;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\Extension\LeanXcale\DBAL\Platforms\LeanXcalePlatform;

class LeanXcalePlatformTest extends AbstractPlatformTestCase
{

    /**
     * @inheritDoc
     */
    public function createPlatform(): AbstractPlatform
    {
        return new LeanXcalePlatform();
    }


    public function testReturnsBinaryTypeDeclarationSQL(): void
    {
        self::assertSame('BLOB', $this->platform->getBinaryTypeDeclarationSQL([]));
        self::assertSame('BLOB', $this->platform->getBinaryTypeDeclarationSQL(['length' => 0]));
        self::assertSame('BLOB', $this->platform->getBinaryTypeDeclarationSQL(['length' => 9999999]));

        self::assertSame('BINARY', $this->platform->getBinaryTypeDeclarationSQL(['fixed' => true]));
        self::assertSame('BINARY', $this->platform->getBinaryTypeDeclarationSQL(['fixed' => true, 'length' => 0]));
        self::assertSame('BLOB', $this->platform->getBinaryTypeDeclarationSQL(['fixed' => true, 'length' => 9999999]));
    }

    public function getGenerateTableSql(): string
    {
        return 'CREATE TABLE test (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, test VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))';
    }

    /**
     * @inheritDoc
     */
    public function getGenerateTableWithMultiColumnUniqueIndexSql(): array
    {
        return [
            'CREATE TABLE test (foo VARCHAR(255) DEFAULT NULL, bar VARCHAR(255) DEFAULT NULL)',
            'CREATE UNIQUE INDEX UNIQ_D87F7E0C8C73652176FF8CAA ON test (foo, bar)'
        ];
    }

    public function getGenerateIndexSql(): string
    {
        return "CREATE INDEX my_idx ON mytable (user_name, last_login)";
    }

    public function getGenerateUniqueIndexSql(): string
    {
        return "CREATE UNIQUE INDEX index_name ON test (test, test2)";
    }

    /**
     * @inheritDoc
     */
    public function getGenerateAlterTableSql(): array
    {
        return [
            "ALTER TABLE mytable ADD COLUMN quota INT DEFAULT NULL",
            "ALTER TABLE mytable DROP COLUMN foo",
            "alter table mytable alter column  bar VARCHAR(255) NOT NULL DEFAULT 'def'",
            "alter table mytable alter column  bloo BOOLEAN NOT NULL DEFAULT 0",
            "ALTER TABLE mytable RENAME TO userlist"
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAlterTableRenameColumnSQL(): array
    {
        return [
            'ALTER TABLE foo ADD COLUMN baz INT NOT NULL DEFAULT 666',
            'UPDATE foo SET baz = cast("bar" as INT)',
            'ALTER TABLE foo DROP COLUMN "bar"',
        ];
    }

    public function testReturnsGuidTypeDeclarationSQL(): void
    {
        self::assertSame('CHAR(36)', $this->platform->getGuidTypeDeclarationSQL([]));
    }

    protected function getGenerateForeignKeySql(): string
    {
        return "ALTER TABLE test ADD FOREIGN KEY (fk_name_id) REFERENCES other_table (id)";
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedColumnInPrimaryKeySQL(): array
    {
        return ['CREATE TABLE "quoted" ("create" VARCHAR(255) NOT NULL, PRIMARY KEY("create"))'];
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedColumnInIndexSQL(): array
    {
        return ["CREATE TABLE \"quoted\" (\"create\" VARCHAR(255) NOT NULL)",
            "CREATE INDEX IDX_22660D028FD6E0FB ON \"quoted\" (\"create\")"];
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedNameInIndexSQL(): array
    {
        return [
            "CREATE TABLE test (column1 VARCHAR(255) NOT NULL)",
            "CREATE INDEX \"key\" ON test (column1)"
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedColumnInForeignKeySQL(): array
    {
        return [
            "CREATE TABLE \"quoted\" (\"create\" VARCHAR(255) NOT NULL, foo VARCHAR(255) NOT NULL, \"bar\" VARCHAR(255) NOT NULL)",
            "CREATE INDEX IDX_22660D028FD6E0FB8C736521D79164E3 ON \"quoted\" (\"create\", foo, \"bar\")",
            "ALTER TABLE \"quoted\" ADD CONSTRAINT FK_WITH_RESERVED_KEYWORD FOREIGN KEY (\"create\", foo, \"bar\") REFERENCES \"foreign\" (\"create\", bar, \"foo-bar\")",
            "ALTER TABLE \"quoted\" ADD CONSTRAINT FK_WITH_NON_RESERVED_KEYWORD FOREIGN KEY (\"create\", foo, \"bar\") REFERENCES foo (\"create\", bar, \"foo-bar\")",
            "ALTER TABLE \"quoted\" ADD CONSTRAINT FK_WITH_INTENDED_QUOTATION FOREIGN KEY (\"create\", foo, \"bar\") REFERENCES \"foo-bar\" (\"create\", bar, \"foo-bar\")"];
    }

    protected function getQuotesReservedKeywordInUniqueConstraintDeclarationSQL(): string
    {
        return 'CONSTRAINT "select" UNIQUE (foo)';
    }

    protected function getQuotesReservedKeywordInTruncateTableSQL(): string
    {
        return 'TRUNCATE TABLE "select"';
    }

    protected function getQuotesReservedKeywordInIndexDeclarationSQL(): string
    {
        return 'INDEX "select" (foo)';
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedAlterTableRenameColumnSQL(): array
    {
        return [

            'ALTER TABLE mytable ADD COLUMN unquoted INT NOT NULL',
            'UPDATE mytable SET unquoted = cast("unquoted1" as INT)',
            'ALTER TABLE mytable DROP COLUMN "unquoted1"',
            'ALTER TABLE mytable ADD COLUMN "where" INT NOT NULL',
            'UPDATE mytable SET "where" = cast("unquoted2" as INT)',
            'ALTER TABLE mytable DROP COLUMN "unquoted2"',
            'ALTER TABLE mytable ADD COLUMN "foo" INT NOT NULL',
            'UPDATE mytable SET "foo" = cast("unquoted3" as INT)',
            'ALTER TABLE mytable DROP COLUMN "unquoted3"',
            'ALTER TABLE mytable ADD COLUMN reserved_keyword INT NOT NULL',
            'UPDATE mytable SET reserved_keyword = cast("create" as INT)',
            'ALTER TABLE mytable DROP COLUMN "create"',
            'ALTER TABLE mytable ADD COLUMN "from" INT NOT NULL',
            'UPDATE mytable SET "from" = cast("table" as INT)',
            'ALTER TABLE mytable DROP COLUMN "table"',
            'ALTER TABLE mytable ADD COLUMN "bar" INT NOT NULL',
            'UPDATE mytable SET "bar" = cast("select" as INT)',
            'ALTER TABLE mytable DROP COLUMN "select"',
            'ALTER TABLE mytable ADD COLUMN quoted INT NOT NULL',
            'UPDATE mytable SET quoted = cast("quoted1" as INT)',
            'ALTER TABLE mytable DROP COLUMN "quoted1"',
            'ALTER TABLE mytable ADD COLUMN "and" INT NOT NULL',
            'UPDATE mytable SET "and" = cast("quoted2" as INT)',
            'ALTER TABLE mytable DROP COLUMN "quoted2"',
            'ALTER TABLE mytable ADD COLUMN "baz" INT NOT NULL',
            'UPDATE mytable SET "baz" = cast("quoted3" as INT)',
            'ALTER TABLE mytable DROP COLUMN "quoted3"',

        ];
    }

    /**
     * @inheritDoc
     */
    protected function getQuotedAlterTableChangeColumnLengthSQL(): array
    {
        return [

            "ALTER TABLE mytable ADD COLUMN TEMP_69C017554AF72489 VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET TEMP_69C017554AF72489 = cast(\"unquoted1\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"unquoted1\""
            , "ALTER TABLE mytable ADD COLUMN unquoted1 VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET unquoted1 = cast(TEMP_69C017554AF72489 as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN TEMP_69C017554AF72489"
            , "ALTER TABLE mytable ADD COLUMN TEMP_F0C946EF61DA774A VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET TEMP_F0C946EF61DA774A = cast(\"unquoted2\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"unquoted2\""
            , "ALTER TABLE mytable ADD COLUMN unquoted2 VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET unquoted2 = cast(TEMP_F0C946EF61DA774A as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN TEMP_F0C946EF61DA774A"
            , "ALTER TABLE mytable ADD COLUMN TEMP_87CE767978C1460B VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET TEMP_87CE767978C1460B = cast(\"unquoted3\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"unquoted3\""
            , "ALTER TABLE mytable ADD COLUMN unquoted3 VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET unquoted3 = cast(TEMP_87CE767978C1460B as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN TEMP_87CE767978C1460B"
            , "ALTER TABLE mytable ADD COLUMN \"create\" VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET \"create\" = cast(\"create\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"create\""
            , "ALTER TABLE mytable ADD COLUMN \"table\" VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET \"table\" = cast(\"table\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"table\""
            , "ALTER TABLE mytable ADD COLUMN \"select\" VARCHAR(255) NOT NULL"
            , "UPDATE mytable SET \"select\" = cast(\"select\" as VARCHAR(255))"
            , "ALTER TABLE mytable DROP COLUMN \"select\"",];
    }

    /**
     * @inheritDoc
     */
    protected function getCommentOnColumnSQL(): array
    {
        return ['', '', '',];
    }

    protected function getQuotedCommentOnColumnSQLWithoutQuoteCharacter(): string
    {
        return "";
    }

    protected function getQuotedCommentOnColumnSQLWithQuoteCharacter(): string
    {
        return "";
    }

    /**
     * @inheritDoc
     */
    protected function getQuotesTableIdentifiersInAlterTableSQL(): array
    {
        return [
            'ALTER TABLE "foo" DROP CONSTRAINT fk1'
            , 'ALTER TABLE "foo" DROP CONSTRAINT fk2'
            , 'ALTER TABLE "foo" ADD COLUMN bloo INT NOT NULL'
            , 'ALTER TABLE "foo" DROP COLUMN baz'
            , 'ALTER TABLE "foo" ADD COLUMN TEMP_76FF8CAA6D704F76 INT DEFAULT NULL'
            , 'UPDATE "foo" SET TEMP_76FF8CAA6D704F76 = cast("bar" as INT)'
            , 'ALTER TABLE "foo" DROP COLUMN "bar"'
            , 'ALTER TABLE "foo" ADD COLUMN bar INT DEFAULT NULL'
            , 'UPDATE "foo" SET bar = cast(TEMP_76FF8CAA6D704F76 as INT)'
            , 'ALTER TABLE "foo" DROP COLUMN TEMP_76FF8CAA6D704F76'
            , 'ALTER TABLE "foo" ADD COLUMN war INT NOT NULL'
            , 'UPDATE "foo" SET war = cast("id" as INT)'
            , 'ALTER TABLE "foo" DROP COLUMN "id"'
            , 'ALTER TABLE "foo" RENAME TO "table"'
            , 'ALTER TABLE "table" ADD CONSTRAINT fk_add FOREIGN KEY (fk3) REFERENCES fk_table (id)'
            , 'ALTER TABLE "table" ADD CONSTRAINT fk2 FOREIGN KEY (fk2) REFERENCES fk_table2 (id)'
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getAlterStringToFixedStringSQL(): array
    {
        return [
            "ALTER TABLE mytable ADD COLUMN TEMP_5E237E06D3DBDAA1 CHAR(2) NOT NULL",
            "UPDATE mytable SET TEMP_5E237E06D3DBDAA1 = cast(\"name\" as CHAR(2))",
            "ALTER TABLE mytable DROP COLUMN \"name\"",
            "ALTER TABLE mytable ADD COLUMN name CHAR(2) NOT NULL",
            "UPDATE mytable SET name = cast(TEMP_5E237E06D3DBDAA1 as CHAR(2))",
            "ALTER TABLE mytable DROP COLUMN TEMP_5E237E06D3DBDAA1"];
    }

    protected function getQuotesDropForeignKeySQL(): string
    {
        return 'ALTER TABLE "table" DROP CONSTRAINT "select"';
    }

    /**
     * @inheritDoc
     */
    protected function getGeneratesAlterTableRenameIndexUsedByForeignKeySQL(): array
    {
        return [
            'DROP INDEX idx_foo',
            'CREATE INDEX idx_foo_renamed ON mytable (foo)'];
    }
}